// console.log("Hello");

// [SECTION] While Loop
// Iteration is the term given to the repition of statements

let count = 5;

while(count !== 0) {
    console.log("While: " + count);
    count--; // (--) --> decrementation, decreasing the value by 1
}


// [SECTION] Do-While Loop
let number = Number(prompt("Give me a number"));

do {
    console.log("Do While: " + number);

    // Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
    number += 1;
} while(number <= 10);

// [SECTION] For Loops
// A For Loop is more flexible than while and do-while loops. It has 3 parts: Initialization value that will track the progression of the loop; Expression/condition that will evaluated which will determine whether the loop will run one more time; finalExpression indicates how to advance the loop.

/*  for (initialization; expression/condition; finalExpression) {
     statement
} */

for (let count = 0; count <= 1; count++) {
    console.log(count);
}

// for (let count = 0; count <= 39; count++) {
//     console.log(count);
// }

let myString = "Alex"; 

console.log(myString.length);

console.log(myString[0]);
// console.log(myString[-1]); can be used for looking for the last array lol

for(let x = 0; x < myString.length; x++) {
    console.log(myString[x]);
}

// Create a string named "myName" with a value of your name

let myName = "Shawn";

for(let i = 0; i < myName.length; i++) {
    // console.log(myName[i].toLowerCase());

    if(
        myName[i].toLowerCase() == "a" || 
        myName[i].toLowerCase() == "i" || 
        myName[i].toLowerCase() == "e" || 
        myName[i].toLowerCase() == "o" || 
        myName[i].toLowerCase() == "u"  
        ) {
            console.log(3);
        } else {
            console.log(myName[i]);
        }
}


// [SECTION] Continue and Break Statements

for (let count = 0; count <= 20; count++) {
    if(count % 2 === 0){
        continue; // tells the code to continue to the next iteration
    }
    console.log("Continue and break: " + count);
    // tells the code to terminate/stop
    if (count > 10) {
        break;
    }
}

let name = "alexandro"

for (let i = 0; i < name.length; i++) {
    console.log(name[i]);

    if(name[i].toLowerCase() === "a") {
        console.log("Continue to the next iteration");
        continue;
    }
    if(name[i] == "d") {
        break;
    }

}